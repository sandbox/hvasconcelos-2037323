-- SUMMARY --
	
	This module aims to list nodes which are using absolute links to referrence 
	any internal pages. This is particularly useful for when a site is being 
	worked on on a development environment, where it's possible that the user 
	might copy and paste a link directly into some of the created content. 
	Once that site is moved to the production environment, the absolute link 
	would still referrence the development server. 
	
	This module will find all of the absolute links and make them relative, 
	therefore avoiding this issue altogether.
	
-- REQUIREMENTS --

	None.

-- INSTALLATION --

	Place this module in either the sites/all/modules or sites/default/modules 
	folder and then enable it. 
	
	For more instructions: 
	https://drupal.org/documentation/install/modules-themes

-- CONFIGURATION --

	You can access this module by navigating to your site's configuration page 
	at admin/structure or directly at admin/structure/fix_absolute_links

-- CONTACT --

	Helen M. Vasconcelos - https://drupal.org/user/2409556
	
	
/// Coalmarch Productions - www.coalmarch.com ///

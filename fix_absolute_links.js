(function ($) {
  Drupal.behaviors.fix_absolute_links = {
    attach: function (context, settings) {

      $('#selectAll').toggle(
        function(e) {
          e.preventDefault();
          $('.form-checkboxes input[type="checkbox"]').attr('checked',true);
          $('#selectAll').text('deselect all');
      },
        function(e) {
          e.preventDefault();
          $('#selectAll').text('select all');
          $('.form-checkboxes input[type="checkbox"]').attr('checked',false);
        }
      );
    }
  };
}(jQuery));
